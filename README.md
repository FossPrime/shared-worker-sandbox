:smaug made a cool tool at https://github.com/smaug----/serviceworkerconsole
that lets you easily let you eval() code in a ServiceWorker.  This is the
logical expansion of that to also include DedicatedWorkers and SharedWorkers.

There are very likely many other tools like this one, but it turns out to be
hard to search for it.