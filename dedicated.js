const state = {
  log: '',
  after: ''
} 
const REAL_LOG = console.log.bind(console)
console.log = function (first, ...last) {
  REAL_LOG(first)
  state.log += first +  '\n'
}

addEventListener("message", messageHandler)

function messageHandler(evt) {
  let { generation, str } = evt.data;
  let result = ''

  if(str === '[temporary/done]') {
    console.log('yeet')
    try {
      result += eval(state.after)
      result += '\n----------------\n'
      result += state.log
    } catch (ex) {
      result = "Exception: " + ex
    }
  } else {
    state.after = str
    state.log = ''
    temporary()
  }

  postMessage({
    generation,
    result
  })
}

// CUSTOM "TEMPORARY" CODE:
async function temporary() {
  const readableStream = new ReadableStream({
    start(controller) {
      // Called by constructor.
      console.log('[start]');
      controller.enqueue('a');
      controller.enqueue('b');
      controller.enqueue('c');
    },
    pull(controller) {
      // Called `read()` when the controller's queue is empty.
      console.log('[pull]');
      controller.enqueue('d');
      controller.close();
    },
    cancel(reason) {
      // Called when the stream is canceled.
      console.log('[cancel]', reason);
    },
  });

  // Create two `ReadableStream`s.
  const [streamA, streamB] = readableStream.tee();

  // Read streamA iteratively one by one. Typically, you
  // would not do it this way, but you certainly can.
  const readerA = streamA.getReader();
  console.log('[A]', await readerA.read()); //=> {value: "a", done: false}
  console.log('[A]', await readerA.read()); //=> {value: "b", done: false}
  console.log('[A]', await readerA.read()); //=> {value: "c", done: false}
  console.log('[A]', await readerA.read()); //=> {value: "d", done: false}
  console.log('[A]', await readerA.read()); //=> {value: undefined, done: true}

  // Read streamB in a loop. This is the more common way
  // to read data from the stream.
  const readerB = streamB.getReader();
  while (true) {
    const result = await readerB.read();
    if (result.done) break;
    console.log('[B]', result);
  }
  
  
}