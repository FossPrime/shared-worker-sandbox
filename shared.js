// This can be shared between unrealated windows

const createdAt = Date.now()
const createdAtStr = `Created on: ${createdAt}\n`
const ports = []
let sm = {
  connections: 1,
  messages: 0
}

addEventListener("connect", function(e) {
  const connectedAt = Date.now()
  const port = e.ports[0]
  ports.push({p: port, pretty: ''})
  console.log('Connected ' + ports.length)
  
  port.addEventListener("message", function(evt) {
    const messagedAt = Date.now()
    let { generation, str } = evt.data;
    sm.messages++
    
    let multi = ''
    try {
      const single = eval(str) + ""
      ports.forEach((portObj,i) => {
        if (port === portObj.p) {
          portObj.pretty = `#${i}: ${single}`
        }
        if (portObj.pretty !== '') {
          multi += portObj.pretty + '\n'
        }
      })
    } catch (ex) {
      const single = "Exception: " + ex
      ports.forEach((portObj,i) => {
        if (port === portObj.p) {
          portObj.pretty = `#${i}: ${single}`
          multi += portObj.pretty + '\n'
        } else if (portObj.pretty !== '') {
          multi += portObj.pretty + '\n'
        }
      })
    }
    
    postMultiMsg(multi, messagedAt, generation)
  });
  
  port.start();
  
  let initAgg = ''
  ports.forEach((portObj,i) => {
    if (portObj.pretty !== '') {
      initAgg += portObj.pretty + '\n'
    }
  })
  postMultiMsg(initAgg)
});

function postMultiMsg (multi, at, gen) {
  ports.forEach((portObj, index) => {
    portObj.p.postMessage({
      generation: gen || 0,
      result: createdAtStr +
        `Messages: ${sm.messages}\n` +
        multi + 
       `Finished in: ${Date.now() - (at || createdAt)}`
    })
  })
}